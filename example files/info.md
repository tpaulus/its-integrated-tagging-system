#Usage
In this folder there are files with the same name as the script would like them to be, as well as the same formatting.
Simply move the files out of this directory and change the contents with your data.

Or if you are adventurous, make some modifications to the file parser, most of which lives in ```load.py```

#Files
##Sales Records
+ ```sales.csv```
+ The accounting office provided me with a list of the Students First and Last name, their ID Number and if they bought
a book that was Namestamped.

##New Sales
+ ```new.csv```
+ This lists is just like the sales list, but it includes all students who bought a book __after__ the original sales
list was generated.

##Net-ID
+ ```Net-ID_List.csv```
+ Accounting did not provide me with the student's grade level, but I was able to get a list matching the student's
Network ID (which is composed of the first letter of their first name, first three letters of their last name, and the
last four digits of their ID Number).

##Name Stamping
+ ```wpc.txt```
+ The Yearbook publishing company provided me a list of all students who's names had been uploaded to their NameStamping
 system. This list is __not__ the same as the Sales list became we provided some of our staff with free namestamped books.