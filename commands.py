__author__ = 'tpaulus'

import os
import shutil
import time
import json
import csv
from objects import Student
from box import Box
from tag import Tag
from load import LoadFile, Grade, AddNew


class Functions(object):
    @staticmethod
    def backup(db='./db.json'):
        """
        Make a backup of the Database.
        Should be done before saving.
        :param db: Location of the primary database
        :type db: str
        """
        if not os.path.isdir("./db_backups"):
            os.makedirs("./db_backups")

        if os.path.exists('./db.json'):
            backup_file_name = './db_backups/%s.json' % str(int(time.time()))
            with open(db) as load_file:
                with open(backup_file_name, mode='w') as save_file:
                    save_file.write(load_file.read())

    @staticmethod
    def save(data_in, db='./db.json'):
        """
        Save Students to the DB file

        :param data_in: List of Student Objects to be saved to the Database
        :type data_in: list
        :param db: Path to the DataBase File
        :type db: str
        :return: If the operation was completed successfully
        :rtype: bool
        """

        try:
            Functions.backup()

        except Exception, e:
            print("Error backing up DB... Not Critical, but important to know.")
            print e

        finally:
            with open(db, mode='w') as save_file:
                data = '['
                for entry in data_in:
                    try:
                        data += json.dumps(entry.dict()) + ',\n'
                    except AttributeError:
                        data += json.dumps(entry) + ',\n'
                data = data[:-2:] + ']\n'
                save_file.write(data)

    @staticmethod
    def load():
        """
        Load Records

        :return: If the file was loaded successfully
        :rtype: bool
        """
        global student_list
        student_list = list()

        try:
            student_list = LoadFile.make_list('./wpc.txt', './sales.csv')
            Functions.save(student_list)
            return True

        except Exception, e:
            print "FATAL: %s" % e
            return False, e

    @staticmethod
    def load_new():
        """
        Load new Sales

        :return: If was completed successful
        :rtype: bool
        """
        global student_list

        student_list = AddNew(student_list).load_new('./new.csv')

        Functions.save(student_list)
        return True

    @staticmethod
    def read(db='./db.json'):
        """
        Read Students from the saved DB file

        :param db: Path to the DataBase File
        :type db: str
        :return: List of students read from the database file
        :rtype: list
        """
        global student_list
        read_student_list = list()

        with open(db) as read_file:
            line = read_file.readline().replace('[', '')[:-2:]
            while line != '':
                # print line
                read_data = json.loads(line)

                grade = read_data['grade']
                first_name = read_data['name']['first']
                last_name = read_data['name']['last']
                namestamp = read_data['namestamp']
                special = read_data['special']
                tag_batch = read_data['batch']
                book_tag = read_data['book tag']
                box = read_data['box']
                stu_id = read_data['id']
                notes = read_data['notes']

                read_student_list.append(Student(first_name, last_name, grade, stu_id, namestamp, special=special,
                                                 batch=tag_batch, book_tag=book_tag, box=box, notes=notes))

                line = read_file.readline().replace('[', '').replace(']', ',')[:-2:]
                # -2, because 1 is the new line, the other is the comma for the list

        print "Read %i Records" % len(read_student_list)

        return read_student_list

    @staticmethod
    def find(search_query):
        """
        Locate a Student in the DB that is loaded into memory (student_list)

        :param search_query: The parameter to search for
        :type search_query: str
        :return: list of Student objects whose name matched the search name
        :rtype: list
        """
        return_list = list()

        # Parse Query
        try:
            param, query = search_query.split(":")

        except ValueError:
            param = search_query
            query = ''

        while param.endswith(' '):
            param = param[:-1].lower()
        while query.startswith(' '):
            query = query[1:]

        if param == '':
            print("Please include a search parameter.")
            return return_list

        if query == '':
            print("Please include a search query after the parameter, separated by a colon.")
            return return_list

        if param == 'name':
            print("Please use wither 'first_name' or 'last_name' respectively to find who you are looking for.")
            return return_list

        # Do Search
        for entry in student_list:
            if param == 'first_name':
                if entry.first_name == query:
                    return_list.append(entry)

            elif param == 'last_name':
                if entry.last_name == query:
                    return_list.append(entry)

            elif param == 'grade':
                if str(entry.grade) == query:
                    return_list.append(entry)

            elif param == 'id':
                if entry.id == query:
                    return_list.append(entry)

            elif param == 'tag':
                try:
                    if entry.tag_id == query:
                        return_list.append(entry)
                except TypeError or AttributeError or Exception:
                    # Tags don't exist yet
                    pass

            elif param == 'batch':
                try:
                    if entry.tag_batch == query:
                        return_list.append(entry)
                except TypeError or AttributeError or Exception:
                    # Tags don't exist yet
                    pass

            elif param == 'box':
                try:
                    if entry.box == query:
                        return_list.append(entry)
                except TypeError or AttributeError or Exception:
                    # Books haven't been sorted yet
                    pass

            elif param == 'namestamp':
                if query.lower() == 'true' and entry.namestamp:
                    return_list.append(entry)

            elif param == 'special':
                if query.lower() == 'true' and entry.special:
                    return_list.append(entry)

        return return_list

    @staticmethod
    def generate_batch_code():
        """
        Make batch code for Tags

        :return: Batch Code
        :rtype: str
        """
        time_stamp = time.strftime('%m%d%H%M%S')
        return time_stamp

    @staticmethod
    def edit_student(stu):
        """
        Modify a Student Object, or create a new one if stu is None

        :param stu: Student Object to Modify
        :type stu: Student
        :return: If was completed Successfully.
        :rtype: bool
        """
        global student_list

        while True:
            if stu is None:
                stu = Student("", "", 0, 0, False)
                new_student = True
            else:
                new_student = False

            # First Name
            input_first = raw_input("First Name [%s]:" % stu.first_name)
            if input_first == "":
                new_first = stu.first_name
            else:
                new_first = input_first

            # Last Name
            input_last = raw_input("Last Name [%s]:" % stu.last_name)
            if input_last == "":
                new_last = stu.last_name
            else:
                new_last = input_last

            # Duplicate Check, by Name
            if len(Functions.find('last_name: %s' % new_last)) != 0 and len(
                    Functions.find('first_name: %s' % new_first)) != 0 and new_student:
                print("Warning, A Student with that First and Last name combination already is in the DB.")

            # ID
            input_id = raw_input("Student ID [%s]:" % str(stu.id))
            if input_id == "":
                new_id = stu.id
            else:
                new_id = input_id

            # Duplicate Check, by ID
            if len(Functions.find('id:%s' % new_id)) > 0 and new_id != 0 and new_id != 'STA' and new_id != stu.id:
                print("Warning, A Student with that ID Number already exists")

            # Grade
            grade_list = Grade('./Net-ID_List.csv')
            new_grade = grade_list.by_net_id(grade_list.make_net_id(new_first, new_last, new_id))

            input_grade = raw_input("Grade [%s]:" % str(new_grade))
            if input_grade != "":
                try:
                    new_grade = int(input_grade)
                except TypeError or AttributeError or Exception:
                    new_grade = input_grade

            # Name Stamp
            if stu.namestamp:
                namestamp_string = 'Yes'
            else:
                namestamp_string = 'No'

            input_ns = raw_input("Name Stamp [%s]:" % namestamp_string)
            if input_ns == "":
                new_ns = stu.namestamp
            elif input_ns.lower() == 'yes':
                new_ns = True
            else:
                new_ns = False

            # Special
            if stu.special:
                special_string = 'Yes'
            else:
                special_string = 'No'

            input_special = raw_input("Special [%s]:" % special_string)
            if input_special == "":
                new_special = stu.special
            elif input_special.lower() == 'yes':
                new_special = True
            else:
                new_special = False

            # Notes
            input_notes = raw_input("Notes [%s]:" % stu.notes_str)
            if input_notes == "":
                notes = stu.notes_str
            else:
                notes = input_notes

            # Book Tag and Boxes
            new_tag = None
            if new_student:
                input_tag = raw_input("Tag ID []: ")
                if input_tag == "":
                    new_tag = None
                else:
                    new_tag = input_tag

            if stu.tag_id is not None:
                input_tag = raw_input("Remove Tag from DB? [Y/n]")
                if input_tag.lower() == "n":
                    new_tag = stu.tag_id

            conf = raw_input("Is the information above correct? [Y/n]")
            if conf != "n":
                stu.first_name = new_first
                stu.last_name = new_last
                stu.id = new_id
                stu.grade = new_grade
                stu.namestamp = new_ns
                stu.special = new_special
                stu.tag_id = new_tag
                stu.notes_str = notes

                if new_student:
                    # Only append the student if they need to be added.
                    student_list.append(stu)

                Functions.save(student_list)
                print("Information Saved")
                return True

    @staticmethod
    def export_csv(output_file_path):
        """
        Export a CSV with all the info that is saved in the Database

        :param output_file_path: Where to save the file to
        :type output_file_path: str
        """
        with open(output_file_path, "w") as output_file:
            csv_file = csv.writer(output_file)
            csv_file.writerow(['Last Name', 'First Name', 'Grade', 'ID', 'Box', 'Tag Number', 'NS', 'Special', 'Notes'])
            for s in sorted(student_list, key=lambda k: str(k.last_name+k.first_name)):
                csv_file.writerow([s.last_name, s.first_name, s.grade, s.id, s.box_id, s.tag_id, s.namestamp,
                                   s.special, s.notes_str])

    def __init__(self):
        pass


class Commands(object):
    @staticmethod
    def help_text():
        """
        Print Help Text

        :return: Help Text
        :rtype: str
        """
        text = 'Welcome to ITS - the Integrated Tagging ans Sorting system for Yearbook and their Distribution.' \
               'This help command is intended to show you the available commands, further details are in the README file.' \
               '\n' \
               '\n' \
               'help\t- Prints this list' \
               'find\t- Locate Student by attribute\n' \
               '\n' \
               'load\t- Load database from source\n' \
               'load new sales\t - Load changes from "new.csv"\n' \
               'read\t- Read from Saved DataBase\n' \
               'dump\t- Print all students currently loaded\n' \
               'export\t- Exports a CSV List with Book/Box Info\n' \
               '\n' \
               "edit\t- Edit a Student's attributes\n" \
               'create new\t- Create a new Student who is added to the DB.\n' \
               '\n' \
               'generate tag\t- Generate Book ID Tags for all students\n' \
               'generate blanks\t- Generate Blank Tags for Distribution Sales\n' \
               'sort books\t- Sort Books into boxes by grade and create box slips\n' \
               'clear print\t- Remove all generated files and IDs from the Database, primarily Tags and Boxes\n' \
               '\n' \
               'quit\t- Exit the CLI'

        return text

    @staticmethod
    def generate_tags():
        """
        Make Tags for Books based on current Database

        :return: Number of Tags Created
        :rtype: int
        """
        global student_list
        batch = Functions.generate_batch_code()
        total_produced = 0
        for student in student_list:
            org_grade = student.grade

            if student.special:
                tag_batch = batch + ' - S'
                student.grade = 'S'
            elif student.namestamp:
                tag_batch = batch + ' - Name'
            else:
                tag_batch = batch

            if student.tag_id is None:
                student.tag_batch = batch
                student.tag_id = Tag(student).generate(tag_batch)
                total_produced += 1

            student.grade = org_grade
        Functions.save(student_list)
        return total_produced

    @staticmethod
    def sort_books():
        """
        Sort Books that have Tags

        :return: Status
        :rtype: str
        """
        global student_list
        student_list = Box(student_list).sort()
        Functions.save(student_list)
        return "Books Sorted"

    @staticmethod
    def clear_tags():
        """
        Remove all Tag and Box Data from DB

        :return: Status
        :rtype: str
        """
        global student_list
        confirmation = raw_input('Are you sure? [y/N] ')
        if confirmation == 'y':
            shutil.rmtree('./tags', ignore_errors=True)
            shutil.rmtree('./boxes', ignore_errors=True)
            for student in student_list:
                student.tag_id = None
                student.tag_batch = None
                student.box_id = None
            Functions.save(student_list)
            return "Tag and Box Information Deleted"

    @staticmethod
    def generate_blank_tags(count):
        """
        Make Blank Tags

        :param count: How many should be generated
        :type count: int
        :return: Status
        :rtype: bool
        """
        batch = Functions.generate_batch_code() + " - Blank"

        for i in range(count):
            # noinspection PyTypeChecker
            Tag(Student('', '', ' ', ' ', False)).generate(batch)

        return True

    def __init__(self):
        pass


try:
    student_list = Functions.read()

except IOError:
    # The DB file doesn't yet exist
    print("Database not yet loaded, consider running 'load' before continuing.")
    student_list = list()
