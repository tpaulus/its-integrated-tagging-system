__author__ = 'tpaulus'

import os
from reportlab.lib.pagesizes import inch
from reportlab.platypus import Table, TableStyle
from reportlab.pdfgen import canvas
from reportlab.lib import colors

from objects import Student


class Box(object):
    box_size = 14   # How many books per box? (Recommended not to be less than the standard number of books per box)

    def __init__(self, student_list):
        self.student_list = student_list

    def sort(self):
        """
        Sort all students by grade

        :return: New Student List with Boxes added to Student Objects
        :rtype: list
        """
        sorted_students = dict()
        for student in self.student_list:
            grade = student.grade

            if student.special:
                grade = 'S'

            try:
                sorted_students[grade].append(student)
            except KeyError:
                sorted_students[grade] = [student]

        boxed_dict = dict()
        for grade in sorted_students.keys():
            boxed_list = list()
            for box_number in range(0, int(len(sorted_students[grade]) / Box.box_size + 1.5)):
                students_in_box = list()
                current_pos = (Box.box_size * box_number)
                for student_number in range(current_pos, current_pos + Box.box_size):
                    try:
                        students_in_box.append(sorted_students[grade][student_number])
                    except IndexError:
                        # End of List
                        break
                boxed_list.append(students_in_box)
            boxed_dict[grade] = boxed_list

        return self.make_pdf(boxed_dict)

    @staticmethod
    def make_pdf(boxed_dict):
        """
        Make the PDFs with the sort information in a table.

        :param boxed_dict: Dictionary with Students sorted by Grade {9: [STU]}
        :type boxed_dict: dict
        :return: New Student List with Boxes added to Student Objects
        :rtype: list
        """
        new_student_list = list()
        if not os.path.isdir('./boxes'):
            os.makedirs('./boxes')

        for grade in boxed_dict.keys():
            c = canvas.Canvas('./boxes/Box Contents - Grade %s.pdf' % str(grade), pagesize=(8.5 * inch, 4.5 * inch))

            current_box = 0
            for box in boxed_dict[grade]:
                current_box += 1
                c.saveState()
                c.setFont('Helvetica-Bold', 42)
                box_id = '%s%s' % (str(grade), chr(ord('A') + current_box - 1))
                c.drawString(272, 270, box_id)
                c.restoreState()

                books = []  # Will be sorted into 2 rows, 1-7 on the left and 8-14 on the right
                for row in range(0, int(Box.box_size / 2 + .5)):
                    try:
                        left_student = box[row]
                        left_text = '%s %s' % (left_student['name']['first'], left_student['name']['last'])
                        if left_student['notes'] != '':
                            left_text += '   - ' + left_student['notes']

                        left_student.box_id = box_id
                        new_student_list.append(left_student)
                    except IndexError:
                        # Less than 14 Books in this box
                        left_text = ''

                    try:
                        right_student = box[row + int(Box.box_size / 2 + .5)]
                        right_text = '%s %s' % (right_student['name']['first'], right_student['name']['last'])
                        if right_student['notes'] != '':
                            right_text += '   - ' + right_student['notes']

                        right_student.box_id = box_id
                        new_student_list.append(right_student)
                    except IndexError:
                        # Less than 14 Books in this box
                        right_text = ''

                    books.append([left_text, right_text])

                t = Table(books, 2 * [3.75 * inch], 7 * [.4 * inch])
                t.setStyle(TableStyle([('FONTSIZE', (0, 0), (-1, -1), 13),
                                       ('FONT', (0, 0), (-1, -1), 'Helvetica'),
                                       ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                                       ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                                       ('GRID', (0, 0), (-1, -1), 0.5, colors.darkgray),
                                       ('BOX', (0, 0), (-1, -1), 0.25, colors.lightgrey),
                                       ]))
                t.wrapOn(c, 8 * inch, 4 * inch)
                t.drawOn(c, 36, 36)
                c.showPage()  # Next Page

            c.save()
        new_student_list.sort(key=lambda x: x.get_name_string())
        return new_student_list


if __name__ == '__main__':
    students = list()
    students.append(Student('Ron', 'Wesley', 9, 0, False))
    students.append(Student('Ginny', 'Wesley', 9, 0, True))
    students.append(Student('Luna', 'Lovegood', 9, 0, True))
    students.append(Student('Fleur', 'Delacour', 9, 0, False))
    students.append(Student('Cornelius', 'Fudge', 9, 0, True))
    students.append(Student('Helga', 'Hufflepuff', 9, 0, True))
    students.append(Student('Victor', 'Krum', 9, 0, True))
    students.append(Student('Teddy', 'Lupin', 9, 0, True))

    students.append(Student('Albus', 'Dumbledore', 0, 0, True, special=True))

    returned_boxed_dict = Box(students).sort()
    print(returned_boxed_dict)
