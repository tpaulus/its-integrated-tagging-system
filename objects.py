__author__ = 'tpaulus'


class Student(object):
    def __init__(self, first_name, last_name, grade, stu_id, namestamp, special=False, batch=None, book_tag=None,
                 box=None, notes=None):
        """
        Define each student who has bought a yearbook and each attribute that defines him/her.

        :type first_name: str
        :type last_name: str
        :type grade: int
        :type stu_id: int    # Almost all IDs will be ints, some will be STA or S though.
        :type namestamp: bool
        :type special: bool
        """
        self.first_name = first_name
        self.last_name = last_name
        self.grade = grade
        self.id = str(stu_id)  # String conversion is necessary here due to inconsistencies.
        self.namestamp = namestamp
        self.special = special
        self.tag_batch = batch
        self.tag_id = book_tag
        self.box_id = box
        self.notes_str = self.notes(notes)

    def __repr__(self):
        return str(self.dict())

    def __str__(self):
        return str(self.dict())

    def __getitem__(self, item):
        if item == 'dict':
            return self.dict()
        if item == 'name':
            return {'first': self.first_name, 'last': self.last_name}
        if item == 'notes':
            return self.notes_str
        if item == 'dict_save':
            return self.dict_save()

    def dict(self):
        """
        :return: Dictionary with all student attributes
        :rtype: dict
        """
        return {'name': {'first': self.first_name, 'last': self.last_name},
                'grade': self.grade,
                'namestamp': self.namestamp,
                'special': self.special,
                'batch': self.tag_batch,
                'book tag': self.tag_id,
                'box': self.box_id,
                'id': self.id,
                'notes': self.notes_str}

    def dict_save(self):
        """
        :return: Dictionary with all student attributes minus those that are auto generated
        :rtype: dict
        """
        return {'name': {'first': self.first_name, 'last': self.last_name},
                'grade': self.grade,
                'namestamp': self.namestamp,
                'special': self.special,
                'batch': self.tag_batch,
                'book tag': self.tag_id,
                'id': self.id,
                'box': self.box_id}

    def readable(self):
        """
        :return: A pretty human readable string, used for search
        :rtype: str
        """
        if self.grade == 0:
            grade = "Unknown"
        else:
            grade = str(self.grade)

        if self.namestamp:
            namestamp = 'Yes'
        else:
            namestamp = 'No'

        if self.special:
            special = 'Yes'
        else:
            special = 'No'

        if self.notes_str == '':
            notes_text = "None"
        else:
            notes_text = self.notes_str
        return_string = "%s %s\n\tGrade: %s\n\tID: %s\n\tBook is NameStamped: %s\n\tThis Book is Special: %s" \
                        "\n\tThis Book has Batch/Tag: %s/%s\n\tYou can find it in box: %s\n\tNotes: %s\n" % \
                        (self.first_name, self.last_name, grade, str(self.id), namestamp, special, self.tag_batch,
                         self.tag_id, self.box_id, notes_text)

        return return_string

    def notes(self, param):
        """
        Generate Notes
        :param param: User-input notes
        :return: Notes for Tag/Recipient
        :rtype: str
        """
        attributes = []
        if param is None:
            if not self.namestamp:
                attributes.append('No NS')

            if len(attributes) != 0:
                text = ''
                for a in attributes:
                    text += a
                return text
            else:
                return ''
        else:
            return param

    def get_name_string(self):
        """
        Used for Sorting
        :return: Student's last name plus their first name
        :rtype: str
        """

        return self.last_name + self.first_name
