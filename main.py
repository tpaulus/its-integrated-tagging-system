__author__ = 'tpaulus'

from commands import *

if __name__ == '__main__':
    while True:
        command = raw_input('>').lower()
        if command.count('help') >= 1:
            print(Commands.help_text())

        elif command == 'find':
            query = raw_input("Query >  ")
            find_result = Functions.find(query)

            if len(find_result) == 0:
                print("No results found")

            for student in find_result:
                print(student.readable())

        elif command == 'load':
            confirmation = raw_input('Are you sure? [y/N] ')
            if confirmation == 'y':
                load_result = Functions.load()

                if load_result:
                    print('File Loaded Successfully!')
                else:
                    print('An error occurred while loading your file.')

        elif command == 'load new sales':
            confirmation = raw_input('Are you sure? [y/N] ')
            if confirmation == 'y':
                load_result = Functions.load_new()

                if load_result:
                    print('File Loaded Successfully!')
                else:
                    print('An error occurred while loading your file.')

        elif command == 'read':
            student_list = Functions.read()
            # for student in student_list:
            # print student.readable()

        elif command == 'dump':
            for student in student_list:
                print(student.readable())

        elif command == 'export':
            Functions.export_csv('./export.csv')
            print("Summary CSV List Exported")

        elif command == 'edit':
            query = raw_input("Query > ")
            find_result = Functions.find(query)

            if len(find_result) > 1:
                print("Please be more specific... Use find to get the ID # of who you are trying to edit.")

            elif len(find_result) == 0:
                print("No results found")

            else:
                Functions.edit_student(find_result[0])

        elif command == 'create new':
            Functions.edit_student(None)

        elif command == 'generate tags':
            confirmation = raw_input('Are you sure? [y/N] ').lower()
            if confirmation == 'y':
                total_produced = Commands.generate_tags()
                print('Book ID Tags Generated for %i students' % total_produced)

        elif command == 'generate blanks':
            try:
                count = int(raw_input('How many? '))
            except ValueError:
                print("Please enter an integer.")

            if Commands.generate_blank_tags(count):
                print('%i Blank tags generated successfully' % count)
            else:
                print("Something went wrong...")

        elif command == 'sort books':
            confirmation = raw_input('Are you sure? [y/N] ').lower()
            if confirmation == 'y':
                print(Commands.sort_books())

        elif command == 'clear print':
            print(Commands.clear_tags())

        elif command == 'quit' or command == 'exit':
            print("Good Bye!")
            break

        elif command == '':
            pass

        else:
            print('%s is not a valid command' % command)
