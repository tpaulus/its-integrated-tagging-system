from __future__ import unicode_literals
# coding= utf-8
__author__ = 'tpaulus'

import os
import uuid
from objects import Student
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.graphics.barcode import code93
from reportlab.platypus import Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, black
from reportlab.lib.enums import TA_RIGHT, TA_LEFT


class Tag(object):
    width = 288.0
    height = 792.0

    def __init__(self, student):
        """
        :type student: Student
        """

        self.student = student
        self.tag_id = str(uuid.uuid4())[:8:].upper()

        if not os.path.isdir("./tags"):
            os.makedirs("./tags")

    def generate(self, batch):
        """
        Generate a Book Tag for the Student with the corresponding Batch Code

        :param batch: Batch Code for the Tag
        :type batch: str
        :return: The Tag ID of the Tag that has been created
        :rtype: str

        """
        if not os.path.isdir("./tags/%s" % batch):
            os.makedirs("./tags/%s" % batch)

        student_info = self.student.dict()
        if student_info['name']['last'] != '':
            file_name = student_info['name']['last'].replace(' ', '_') + \
                student_info['name']['first'][:4:] + '.' + self.tag_id + '.pdf'
            blank = False
        else:
            file_name = self.tag_id + '.pdf'
            blank = True

        student_last_name = student_info['name']['last'].split(' ')[0].split('-')[0]
        student_last_name = student_last_name.replace(' ', '')
        # Taking only first part of hyphenated last name or names with a space in them.
        # The stripping Removes the all but the first part of the last name

        student_first_name = student_info['name']['first'] + '\u0020'

        c = canvas.Canvas('./tags/' + batch + '/' + file_name, pagesize=(Tag.width, Tag.height))

        style_sheet = getSampleStyleSheet()

        style_sheet.add(ParagraphStyle(name='LN-1', fontName='Helvetica', fontSize=38, alignment=TA_RIGHT,
                                       textColor=black, backColor=None, allowOrphans=0, wordWrap=None))

        style_sheet.add(ParagraphStyle(name='LN-2', fontName='Helvetica', fontSize=34, alignment=TA_RIGHT,
                                       textColor=black, backColor=None, allowOrphans=0, wordWrap=None))

        style_sheet.add(ParagraphStyle(name='LN-3', fontName='Helvetica', fontSize=30, alignment=TA_RIGHT,
                                       textColor=black, backColor=None, allowOrphans=0, wordWrap=None))

        style_sheet.add(ParagraphStyle(name='agreement', fontName='Helvetica', fontSize=15, alignment=TA_LEFT,
                                       textColor=black, backColor=None, allowOrphans=0, leading=17))

        # Grade
        c.setFont('Helvetica-Bold', 60)
        c.drawString(28, 725, str(student_info['grade']))

        # Last Name
        last_name = Paragraph(student_last_name, style=style_sheet['LN-1'])
        last_name.wrapOn(c, 2.25 * inch, .75 * inch)
        if last_name.height > 15:
            last_name = Paragraph(student_last_name, style=style_sheet['LN-2'])
            last_name.wrapOn(c, 2.25 * inch, .75 * inch)
            if last_name.height > 15:
                last_name = Paragraph(student_last_name, style=style_sheet['LN-3'])
                last_name.wrapOn(c, 2.25 * inch, .75 * inch)
        last_name.drawOn(c, 260 - last_name.width, 770-last_name.height)

        # First Name
        c.setFont('Helvetica-Oblique', 26)
        c.drawAlignedString(260, 690, student_first_name, '\u0020')

        agreement_text = """<para>We hope you enjoy El Año Volume 94, [RE] View. Our staffers put a lot of hard work into this book.
        Please sign on the line below to certify that you have received your copy of El Año 94. Thanks!</para>"""

        agreement = Paragraph(agreement_text, style_sheet['agreement'])
        agreement.wrapOn(c, 3 * inch, 3 * inch)
        agreement.drawOn(c, 30, 422)

        c.setFont('Helvetica', 15)
        c.drawString(30, 365, 'x__________________________')

        barcode = code93.Standard93(self.tag_id)
        barcode.barWidth = 1.9
        barcode.barHeight = 30
        barcode.drawOn(c, ((Tag.width - barcode.width) / 2), 54)

        c.setFont('Helvetica', 11)
        c.drawString(28, 28, 'Tag: %s' % self.tag_id)
        if not blank:
            c.drawAlignedString(260, 28, '%s, %s   %s' % (student_info['name']['last'], student_info['name']['first'],
                                                          str(student_info['grade'])), str(student_info['grade'])[-1::])

        c.save()  # Save generated tag to PDF

        return self.tag_id


if __name__ == "__main__":
    t = Tag(Student(str('John'), str('Doe'), 20, 0, True))
    generated_id = t.generate(str("TEST"))
    print "Generated Tag:", generated_id
