# Usage
Read ```./example files/info.md``` for file importing information.
Run ```./main.py``` in an interactive shell where you can enter commands into the CLI.
Start off by typing in ```help``` to list all available commands.

## Required Packages
- ReportLab
